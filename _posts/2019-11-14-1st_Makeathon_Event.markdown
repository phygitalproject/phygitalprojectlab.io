---
layout: post
title:  "1st Makeathon Event"
date:   2019-11-14 00:00:00 -0300
location: Lakatamia, Cyprus
categories: news
featured: '/images/2019_11_14-1st_Makeathon_event.png'
excerpt: "Lakatamia Municipality and the University of Nicosia Research Foundation invites you on Saturday 16/11/2019 at the 1st Makeathon Event."
---

# 1st Makeathon Event
**Lakatamia Municipality** and the **University of Nicosia Research Foundation** invites you on Saturday 16/11/2019 at the 1st Makeathon Event. 

Learn how through Makerspaces you can have access to free space and tools. Make your ideas reallity! 

# What is a makerspace?

Makerspaces or hackerspaces are collectively operated laboratories and their members have the ability to use tools and equipment (such as 3D printers, laser cutters, CNCs, etc.) that they usually do not have access to. In makerspaces or hackerspaces, knowledge and skills are transmitter and shared. The various projects that take place in these areas are usually the resut of the collaboration between participants or members of the makerspace commutiry, acting as solidarity communities.

# How can you participate?

Take a closer look, take part and learn about the new Makerspace actions on 16/11/2019 at 2:00 pm - 6:00 pm Saturday and Sunday. Admission will be free to the public

# When

Saturday, 16 November 2019, 14:00-18:00

# Location 

Multifunctional Centre of Lakatamia

# Contact

Athina Kyriacou

Email: athina.kyriacou@lakatamia.org.cy
Tel: 22364046 /97

Marios Podinas - Project Coordinator

Email: marios@gnous.com
Tel: 99842995







[PHYGITAL A3 POSTER ONLINE]({{ "/images/2019_11_14-1st_Makeathon_event-POSTER-ONLINE.png" | prepend: site.baseurl }})


[PHYGITAL NEWSLETTER (EL)]({{ "/images/2019_11_14-1st_Makeathon-NEWSLETTER_1.png" | prepend: site.baseurl }})


[PHYGITAL NEWSLETTER (EN)]({{ "/images/2019_11_14-1st_Makeathon-NEWSLETTER_2.png" | prepend: site.baseurl }})

