---
layout: post
title:  "Report of Training sessions for entrepreneurs agri-food sector."
date:   2020-09-17 00:00:00 -0300
location: Northern Tzoumerka, Greece
categories: news
featured: '/images/2020_09_18-reports_Header.jpg'
excerpt: "Report of Training sessions for entrepreneurs agri-food sector on 'Sustainable Development Agritourism Units' organized within  Phygital project."
---

The training sessions for entrepreneurs in the agri-food sector were successfully completed organized by the Municipality of North Tzoumerka on Monday 14 and Tuesday 15 September in Syrrako and Pramanta in the context of the Phygital project.

 &nbsp;
 
 &nbsp;

[REPORT (EL)]({{ "/assets/files/2020_09_17-report.pdf" | prepend: site.baseurl }})
