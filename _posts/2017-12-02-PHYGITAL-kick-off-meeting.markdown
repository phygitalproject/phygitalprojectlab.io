---
layout: post
title:  "PHYGITAL Kick-off Meeting "
date:   2017-12-02 12:00:00 -0300
location: Athens, Greece
categories: news
featured: '/images/meeting.jpg'
---

The Lead Partner of the PHYGITAL Project is organizing the kick-off meeting in Athens, Greece on 7 & 8 Decemeber 2017. Main issues that are going to be discussed are : 

Introduction of PHYGITAL project partners
Interpersonal familiarization with the PHYGITAL project team
Deep understanding of the project concept, objectives and expected results
In depth analysis of the project activities as a whole as well as of the tasks allocated per partner
Agreement of the critical milestones of the PHYGITAL project
Establishment of the PHYGITAL Project Steering Committee
Establishment of a stable and effective collaboration and communication framework

[Agenda](https://www.interreg-balkanmed.eu/gallery/Files/Project_Events/PHYGITAL/1stProjectMeetingAgenda.pdf)