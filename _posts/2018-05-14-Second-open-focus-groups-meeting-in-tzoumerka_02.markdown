---
layout: post
title:  "Second open Focus Group meeting in Tzoumerka"
date:   2018-05-14 12:00:00 -0300
location: Athens, Greece
categories: news
featured: '/images/Poster_FocusGroup2_featured.png'
---

The second open Focus Group meeting will take place at the cultural center of the Kalentzi municipality on the 14th of May.

Main issues that will be discussed are

* Challenges and cooperative open source implementations for agricultural applications in Northern Tzoumerka
* Dscussion and prioritizing the equipment needs for the under-construction makerspace 

[Invitation (in greek)]({{ "/assets/files/Invitation_FG2.pdf" | prepend: site.baseurl }})

[Press release (in greek)]({{ "/assets/files/D3.2.2.prePressRelease_FocusGroup2.pdf" | prepend: site.baseurl }})

[Agenda (in greek)]({{ "/assets/files/Agenda_FG2.pdf" | prepend: site.baseurl }})