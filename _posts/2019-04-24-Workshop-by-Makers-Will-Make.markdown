---
layout: post
title:  "Workshop by Makers Will Make"
date:   2019-04-24 00:00:00 -0300
location: Nicosia, Cyprus
categories: news
featured: '/images/2019_04_24-eva workshop 2_header.jpg'
excerpt: "Digital Fabrication for the Stage, Costumes, Props and Scenery. A University of Nicosia Researh Foundation workshop for the project Phygital by Eva Korae, Eleana Alexandrou and Arianna Marcoulides"
---

**A**  **Workshop by**  **Makers Will Make** (Eva Korae, Eleana Alexandrou and Arianna Marcoulides) University of Nicosia Research Foundation for the project Phygital


Digital Fabrication for the Stage, Costumes, Props and Scenery

# University of Nicosia Research Foundation


This workshop explores how new fabrication technologies may be utilized for the creation of stage design for contemporary dance performances. Participants will be asked to explore photographs/ forms and digitally translate them into costumes, props and scenery for a performance taking place in autumn 2019 in Limassol and Nicosia. Participants will use our equipment to experiment and bring their thoughts to life with a chance of their work featuring in the performance. Credit will be given to the artists of the chosen work.

**Participation is limited to a maximum of 10 participants.**

**Participants must be literate in design software programmes and will need their laptop for the workshop.**

To register please email: Tselika.e@unic.ac.cy and makerswillmake@gmail.com 

**Include your Name; Email; Mobile number and a Short Bio if available.**

Registrations will take place on a first come first served basis.


## About Makers Will Make

An initiative of bytheway productions is space for designers in Limassol, a place for experimentation, expression and creation, equipped with a large-scale laser cutter which is open to the public.
It aims to be the meeting point of creative people in the wider field of design as the dynamic of space, in line with the modern maker movement, offers countless possibilities for experimental approaches to materials, processes and applications. Design promotion, freedom of experimentation, collaboration and professional networking, become facilitated under one working environment where each creator has the opportunity to draw inspiration, to discuss, experiment and share. Through implementing the contemporary experience of designing and materializing an idea, the visitor/creator participates in the whole process, from the drawing board to controlling the production and aesthetic of the final result. Makers from all fields of design (graphics, interior, fashion, architecture, art are just some of them ...) can re-explore the boundaries of their art.
For more information you can visit the website: [makerswillmake.com/](https://makerswillmake.com/)


## Phygital

**Phygital** is an Interreg V 2014-2020 BalkanMed, EU-funded programme being implemented in Greece, Albania and Cyprus and involves the development of makerspaces one in each country - that will work with the local community. In Cyprus, the project's work is being carried out by the University of Nicosia Research Centre in collaboration with the Municipality of Lakatamia/hack66 and will focus on social art practices exploring the melding of open technology, art and design. The project operates on the basis of the 'design global - manufacture local' model which introduces innovative organisational and business patterns allowing an unprecedented booming of communities engaged in do-it-yourself (DIY) activities. It wishes to support and enhance these local capacities for innovation and utilise the opportunities the decentralised modes of production can create. The Cyprus section of the project examines the importance of makerspace culture in the advancement of contemporary social art and design practices. It delves into the principles of open source projects, software-hardware freedom and bottom-up collaborative structures to explore ways they can be utilised - in line with social art/design practices - to address the needs of the local community. 

**PHYGITAL- Catalysing innovation and entrepreneurship unlocking the potential of emerging production and business models**
- Lead Partner-GFOSS/ Greek Free Open Source Software Society 
- Peer to Peer Alternatives - Greece (a.k.a. "P2P Lab")
- Lakatamia Municipality/ hack66- Cyprus
- Open Labs - Albania
- Municipality of North Tzoumerka- Greece
- University of Nicosia Research Foundation (UNRF)- Cyprus
- The National Center of Folklore Activities- Albania



**Workshop** Saturday, 11 may 2019

10:00 - 15:00

**Location**

Makers Will Make Studio

Afroditis 11

Limassol 3042

Cyprus


**Contact**

For more information you can contact

Phone: +357 99 190890

[![Workshop brochure]({{ "/images/2019_04_24-eva_workshop_2_flyer_by_makers_will_make.jpg" | prepend: site.baseurl }})]({{ "/images/2019_04_24-eva_workshop_2_flyer_by_makers_will_make.jpg" | prepend: site.baseurl }})


[press release - en]({{ "/assets/files/2019_04_24-Workshop_by_Makers_Will_Make_en.pdf" | prepend: site.baseurl }})

[press release - el]({{ "/assets/files/2019_04_24-Workshop_by_Makers_Will_Make_el.pdf" | prepend: site.baseurl }})
