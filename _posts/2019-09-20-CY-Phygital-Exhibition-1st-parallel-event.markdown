---
layout: post
title:  "CY Phygital Exhibition 1st parallel event"
date:   2019-09-20 00:00:00 -0300
location: Nicosia, Cyprus
categories: news
featured: '/images/2019_09_20-exhibition_1st_parallel.jpg'
excerpt: "Phygital Exhibition & Events. Co-organised by the University of Nicosia Research Foundation and Lakatamia Municipality. Implementation partner: Thkio Ppalies. Co-curated by Peter Eramian, Elena Parpa, Evi Tselika. Design by Nico Stephou."
---

# Phygital Exhibition
University of **Nicosia Research Foundation** and the **Lakatamia Municipality** | Implementation partner, **Thkio Ppalies**

# Exhibition Parallel programme of events | Makeathon

The exhibition **"sickle & code"** opens with How to Make Flubber. A lecture-workshop by artist **Nihaal Faizal**

# When

28 September 2019, 17:00-20:00

# Location 

Venue | Thkio Ppalies, Nicosia
(2b Kissamou, Palouriotissa, Nicosia 1040)

# Capacity 

20 participants, age 15+

# Registration

info@thkioppalies.org

+35722842694


"Flubber houses, Flubber cars,
Flubber gum and candy bars,
Flubber shoes for tired feet,
Music with a Flubber beat.
 
Flubber buggies, Flubber jets.
Flubber filter cigarettes.
No more trouble, no more care,
Flubber, Flubber, ev'rywhere."
 
"The Flubber Song" by Richard M. Sherman and Robert B. Sherman
 
 
Drawing from 'how-to-make-flubber' videos available online, this lecture-workshop will explore the history of Flubber, a semi-fictional substance popularized by a series of Disney films between 1961 and 1997, unpacking it as an ideological object.
 
Initially appearing within a short story from 1942, Flubber is a substance that shares an intimate relationship with the various timelines and contexts in which it emerges. Shifting across a variety of roles - from that of an artificial rubber, a substance of military utility, a children's toy, a super-fuel, a substance to control the weather, and a sentient, intelligent being - Flubber's conceptual transformations echo the aspirations and desires of its fictional, as well as commercial, producers.
 
In response to Flubber's slimy materiality and Disney's packaging of it as a friendly and harmless substance, Flubber is today most popularly known as an object of play. As the how-to-videos emphasise, this play is no longer in just the handling of the material, but is also embedded in the processes of making this substance - in playing the role of the inventor. Using common household ingredients, the participants in this workshop will produce batches of Flubber, while exploring its various historical roles, desires, and functions.
 
# Bio

**Nihaal Faizal** is an artist whose work responds to already existing documents from a variety
of technological sources. In the past, these have included stock videos, desktop backgrounds, forged antiques, family photographs, popular films, and online videos, amongst others. In responding to these documents, his work addresses questions around authorship,technology, cultural memory, and materiality.
He recently attended the HomeWorkspace Programme at Ashkal Alwan (Beirut, Lebanon) and founded [Reliable Copy](http://www.reliablecopy.org/) -a publishing house for works, projects, and writing by artists (Bangalore, India). Ongoing and upcoming exhibitions include 'View:India' at the Landskrona Museum (Landskrona, Sweden) and 'Sickle-n-Code' at the Lakatamia Museum of Cultural Heritage (Lakatamia, Cyprus). 


**sickle & code opens on the 24th October 2019, at 18:00 at the Museum of History and Cultural Heritage of Lakatamia, Agias Paraskevis, Lakatamia
[https://goo.gl/maps/suVx2PvRAiuTHYm69](https://goo.gl/maps/suVx2PvRAiuTHYm69).**

sickle & code,  is organized by the University of Nicosia Research Foundation the Municipality of Lakatamia, as part of project Phygital  (an Interreg V 2014-2020 BalkanMed, EU-funded programme) Co-organizer Fine Arts Programme, Department of Design and Multimedia, University of Nicosia. 
Implementation partner, Thkio Ppalies: [https://thkioppalies.org/](https://thkioppalies.org/).

[Facebook: https://www.facebook.com/events/693036681164394/](https://www.facebook.com/events/693036681164394/)


[![Exhibition Flyer]({{ "/images/2019_09_20-flyer-exhibition_1st_parallel.jpg" | prepend: site.baseurl }})]({{ "/images/2019_09_20-flyer-exhibition_1st_parallel.jpg" | prepend: site.baseurl }})


[press release - en]({{ "/assets/files/2019_09_20_flubber_makeathon_info_EN.pdf" | prepend: site.baseurl }})

[press release - el]({{ "/assets/files/2019_09_20_flubber_makeathon_info_EL.pdf" | prepend: site.baseurl }})