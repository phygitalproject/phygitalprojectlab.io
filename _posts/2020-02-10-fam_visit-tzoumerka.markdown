---
layout: post
title:  "Familiarization visit for travel bloggers and journalists in North Tzoumerka"
date:   2020-02-10 00:00:00 -0300
location: Northern Tzoumerka, Greece
categories: news
featured: '/images/2020_02_10-fam_visit-tzoumerka_header.png'
excerpt: "The Municipality of Northern Tzoumerka is pleased to invite you to the fam visit for travel bloggers and journalists in North Tzoumerka, which will take place from March 27 to March 29, 2020."
---

# Familiarization visit

The **Municipality of Northern Tzoumerka** is pleased to invite you to the fam visit for travel bloggers and journalists in North Tzoumerka, which will take place from March 27 to March 29, 2020. This familiarization trip aims to highlight the natural and cultural heritage of the wider area of Northern Tzoumerka through targeted publications and posts on the Social Media and the wider Mass Media. Particular attention will be given to the actions of the Phygital project and in particular the rural makerspace “Tzoumakers” located in Kalentzi, in the Municipality of Northern Tzoumerka, Greece.


# When

March 27-29, 2020

# Location 

Northern Tzoumerka, Greece.

<<<<<<< HEAD
=======
# Contact

Email: info@plano2.gr

Tel: (+30) 2311 821025


>>>>>>> development

The deadline for the submission of applications is Wednesday, **25th February 2020**


<<<<<<< HEAD
# INVITATION
[INVITATION]({{ "/assets/files/2020_02_10-fam_visit-tzoumerka-prosklhsh.pdf" | prepend: site.baseurl }})

# REGISTRATION FORM
=======
# INVITATION (EL)
[INVITATION]({{ "/assets/files/2020_02_10-fam_visit-tzoumerka-prosklhsh.pdf" | prepend: site.baseurl }})

# REGISTRATION FORM (EL)
>>>>>>> development
[REGISTRATION FORM]({{ "/assets/files/2020_02_10-fam_visit-tzoumerka-aithsh.pdf" | prepend: site.baseurl }})


