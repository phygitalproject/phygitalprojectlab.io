---
layout: post
title:  "Workshop by University of Nicosia Research Foundation for the project Phygital"
date:   2019-02-26 00:00:00 -0300
location: Egkomi, Cyprus
categories: news
featured: '/images/2019_03_08_workshop_flyer_by_natalia_rozalia_avlona.jpg'
excerpt: "Open Technologies: Their History, Laws, and Gender. A University of Nicosia Researh Foundation workshop for the project Phygital by Natalia- Rozalia Avlona"
---

**A**  **Workshop by**  **Natalia- Rozalia Avlona** University of Nicosia Research Foundation for the project Phygital


Open Technologies: Their History, Laws, and Gender

# University of Nicosia Research Foundation

## Programme

|  |  |
|:----------------|:-------------|
| 14:30-14:45 | Introduction |
| 14:45-16:15 | From Digital Commons to Platform Economy: A Presentation of their history,<br> legal issues,  politics, and culture|
| 16:15-16:45 | Discussion |
| 16:45-17:15 | Break |
| 17:15-18:15 | Gender and Open Technologies: Feminist perspectives, challenges and potentialities |
| 18:15-19:30 | Workshop "Queering your wiki" |
| 19:30-20:00 | Open Discussion |

_Please bring your laptop with you._

During the last four decades, digital commons have shaped virtual terrains of free, participatory and distributed production of immaterial goods. The invention of  World Wide Web, the Commons Based Peer Production (CBPP),the FOSS andthe Free Culture movements have formed new landscapes of governance and production as well as legal tools and virtual communities which were characterised bythe  "hacker ethos".The values of sharing, openness, collaboration and decentralisation came to the front, as post-capitalist values challenging the notion of "ownership" as the subject matter and heart of the traditional copyright law. On the other hand, during the last decade, the rapid rise of the platform economy has been appropriating the values and structures of "sharing" and "decentralisation", introduced by the digital commons, into an uneven value extraction by the platforms. (netarchical capitalism).


The last years, the emergence of makerspaces as open, community-led spaces, where open source software and hardware are utilised collaboratively by individuals, (Kostakis, Niaros & Drechsler 2017) is re-territorialising thes "digital commoning practices" with the means of  additive and subtractive  manufacturing technologies (3D Printing, CNC machining) in to the "phygital" realm, towards the production of tangible  goods. However, whilst these practices are pioneering the democratisation of technology, dismantling the dominant legal and technical structures, it is not clear yet how inclusive are and can be. Hence, the feminist perspectives of open technologies are to be explored as a method and tool to expose their main challenges and potentialities.
This workshop will provide a systematic overview of this field, focusing on its first part on the historical and legal aspects of the open technologies and on the second part, on the feminist perspectives and challenges that come up by its confrontation with the gender.


## About Natalia-Rozalia Avlona

**Natalia-Rozalia Avlona** is a lawyer and researcher working as a research assistant for the TARGET project at ELIAMEP (Hellenic Foundation for European & Foreign Policy) in Athens.

She studied law at the School of Law of the National and Kapodistrian University of Athens (2006), obtained her Master's Degree in Law (LLM) from King's College London (2007), and followed courses in the department of Cultural Geography at Royal Holloway, University of London and in the department of Curating Contemporary Art at the Royal College of Art. Currently, she is a Phd Candidate at the National Technical University of Athens, and her thesis topic is "Open Technologies: Their Histories, Laws and Genders".

Her expertise is on Digital Commons and the ways that Technology, Law and Gender intersect in this area. 

Avlona has  an international experience working in several  Organizations and European Research Programmes in UK, Belgium and Greece. Among those are the Aristotle University of Thessaloniki, the Organisation of Industrial Property in Greece, the Royal College of Art in London, Abandon Normal Devices in Manchester, the Future Emerging Technologies Department (DG Connect, EU Commission) in Brussels, the General Secretariat for Gender Equality and the GUnet (Greek Universities Network) in Athens.

She is member of the Management Committee of Cost Action CA16121-From Sharing to Caring: Examining Socio-Technical Aspects of the Collaborative Economy (2017-2021).

At the moment she is part of ELIAME's Junior Research Fellows Team, working on TARGET (Taking a Reflexive Approach to Gender Equality for Gender Transformation), a research program that aims to contribute to the advancement of  gender equality in Research and Innovation.

Besides her academic career Avlona has a strong involvement as an activist in the field of digital commons and gender equality, whilst she has run a series of workshops on Wikipedia for the Galleries, Libraries, Archives and Museums (GLAM) sector, co-organised feminist workshops on FOSS at hackerspace.gr and (un)conferences on the commons.The last three years, she is member of the Social Solidarity Economy Zone.


## Phygital

**Phygital** is an Interreg V 2014-2020 BalkanMed, EU-funded programme being implemented in Greece, Albania and Cyprus and involves the development of makerspaces - one in each country - that will work with the local community. In Cyprus, the project's work is being carried out by the University of Nicosia Research Centre in collaboration with the Municipality of Lakatamia/hack66 and will focus on social art practices exploring the melding of open technology, art and design. The project operates on the basis of the "design global - manufacture local" model which introduces innovative organisational and business patterns allowing an unprecedented booming of communities engaged in do-it-yourself (DIY) activities. It wishes to support and enhance these local capacities for innovation and utilise the opportunities the decentralised modes of production can create. The Cyprus section of the project examines the importance of makerspace culture in the advancement of contemporary social art and design practices. It delves into the principles of open source projects, software-hardware freedom and bottom-up collaborative structures to explore ways they can be utilised - in line with social art/design practices - to address the needs of the local community. 

**PHYGITAL- Catalysing innovation and entrepreneurship unlocking the potential of emerging production and business models**
- Lead Partner-GFOSS/ Greek Free Open Source Software Society 
- Peer to Peer Alternatives - Greece (a.k.a. "P2P Lab")
- Lakatamia Municipality/ hack66- Cyprus
- Open Labs - Albania
- Municipality of North Tzoumerka- Greece
- University of Nicosia Research Foundation (UNRF)- Cyprus
- The National Center of Folklore Activities- Albania


**Workshop** Friday, 08 March 2019

14:30 - 20:00

**Location**

Fine Art Building

Michail Georgalla 29

Egkomi

Cyprus

[https://goo.gl/maps/7dAjCLtZBSu](https://goo.gl/maps/7dAjCLtZBSu)


**Contact**

For more information you can contact

Evanthia Tselika

Email: tselika.e@unic.ac.cy

Phone: +35722842694

[![Workshop brochure]({{ "/images/2019_03_08_workshop_flyer_by_natalia_rozalia_avlona.jpg" | prepend: site.baseurl }})]({{ "/images/2019_03_08_workshop_flyer_by_natalia_rozalia_avlona.jpg" | prepend: site.baseurl }})


[press release - en]({{ "/assets/files/2019_02_26_press_release_en.pdf" | prepend: site.baseurl }})

[press release - el]({{ "/assets/files/2019_02_26_press_release_gr.pdf" | prepend: site.baseurl }})
