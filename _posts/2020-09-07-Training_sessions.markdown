---
layout: post
title:  "Training sessions for entrepreneurs agri-food sector"
date:   2020-09-07 00:00:00 -0300
location: Northern Tzoumerka, Greece
categories: news
featured: '/images/2020_09_07-Training_sessions-header.png'
excerpt: "The Municipality of North Tzoumerka invites you to participate in Training sessions for entrepreneurs agri-food sector on 'Sustainable Development Agritourism Units' organized within  Phygital project."
---

The Municipality of North Tzoumerka invites you to participate in Training sessions for entrepreneurs agri-food sector on 'Sustainable Development Agritourism Units' organized within  Phygital project.

# Location

Conference Hall "Kostas Krystallis", Sirrako, Northern Tzoumerka, Greece
Hotel "Orizontes" Pramanta, Northern Tzoumerka, Greece

# When

Sirrako: Monday, September 14 2020, 15:00 - 19:00
Pramanta: Tuesday, September 15 2020, 9:30 - 13:30

# Contact

Alexandros Pazaitis

Email: alex.pazaitis@gmail.com

Mobile: (+30) 6972916707
 
 &nbsp;
 
 &nbsp;

[Invitation Flyer (EL)]({{ "/assets/files/2020_09_07-Training_sessions-invitation_flyer.pdf" | prepend: site.baseurl }})

[Registration Form (EL)]({{ "/assets/files/2020_09_07-Aithsh_summetoxhs.pdf" | prepend: site.baseurl }})
