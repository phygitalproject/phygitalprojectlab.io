---
layout: post
title:  "Article on the 'parathyro.politis.com.cy' online magazine for the organization of a workshop by the University of Nicosia Research Foundation as part of the project PHYGITAL"
date:   2018-10-18 12:00:00 -0300
location: Lakatamia, Cyprus
category: press
---

The ['POLITIS PARATHIRO'](http://parathyro.politis.com.cy/) article devoted to the organization of a workshop by the University of Nicosia Research Foundation as part of the project PHYGITAL that happened on October 11 and 13, 2018.

[Article link](http://parathyro.politis.com.cy/2018/10/ergastiri-gia-to-ergo-phygital-apo-to-erevnitiko-idryma-panepistimiou-lefkosias/)