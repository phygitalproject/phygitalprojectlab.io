---
layout: post
title:  "Report of the 'Wandering' Final Event of the Phygital project."
date:   2020-09-18 00:00:00 -0300
location: Northern Tzoumerka, Greece
categories: news
featured: '/images/2020_09_18-reports_Header.jpg'
excerpt: "Report of the 'Wandering' Final Event of the Phygital project."
---

The "Wandering" Closing Day was successfully implemented on Monday 14 & Tuesday 15 September Phygital project, which wandered in the wider area of the Municipality of North Tzoumerka having as stations in Syrrako, Pramanta, Plaisia and Elliniko.
 
 &nbsp;
 
 &nbsp;

[REPORT (EL)]({{ "/assets/files/2020_09_18-report-periplanomeni.pdf" | prepend: site.baseurl }})
