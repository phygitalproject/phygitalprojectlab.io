---
layout: post
title:  "Fam visit for travel bloggers and journalists in North Tzoumerka"
date:   2020-06-15 00:00:00 -0300
location: Northern Tzoumerka, Greece
categories: news
featured: '/images/2020_06_15-famvisit_Header.jpg'
excerpt: "The Municipality of Northern Tzoumerka is pleased to invite you to the fam visit for travel bloggers and journalists in North Tzoumerka, which will take place from July 17 to July 19, 2020."
---

The Municipality of Northern Tzoumerka is pleased to invite you to the fam visit for travel bloggers and journalists in North Tzoumerka, which will take place from July 17 to July 19, 2020. This familiarization trip aims to highlight the natural and cultural heritage of the wider area of Northern Tzoumerka through targeted publications and posts on the Social Media and the wider Mass Media. Particular attention will be given to the actions of the Phygital project and in particular the rural makerspace “Tzoumakers” located in Kalentzi, in the Municipality of Northern Tzoumerka, Greece.


To apply for this call, please fill in the following application form.

 
The deadline for the submission of applications is Friday, 10 July 2020.





[Application form (EL)]({{ "/assets/files/2020_06_15-Aithsh.pdf" | prepend: site.baseurl }})

[Invitation Brochure (EL)]({{ "/assets/files/2020_06_15-D5.5.1.FAMVISIT_WEB.PDF" | prepend: site.baseurl }})
