---
layout: post
title:  "Invitation for presentation of cultural heritage projects, agricultural production, and social practices in the urban space."
date:   2020-12-01 00:00:00 -0300
location: Online, Greece
categories: news4
featured: '/images/2020_09_09-Wandering-Header.png'
excerpt: "The Phygital project invites makers, collectives and initiatives from Greece and abroad to present their projects in the fields of cultural heritage, agricultural production, and social practices in the urban space as part of the three-day online event organized by the Open Technologies Organization - GFOSS on 14, 15, and 16 December 2020."
---

The Phygital project invites makers, collectives and initiatives from Greece and abroad to present their projects in the fields of cultural heritage, agricultural production, and social practices in the urban space as part of the three-day online event organized by the Open Technologies Organization - GFOSS on 14, 15, and 16 December 2020.


The event will present the main actions and results of the project, focusing on three makerspaces in Greece, Cyprus and Albania and the original solutions built by local communities, based on global digital knowledge, software and design communities.


Each day closes with a section open for co-creation and discussion with participants. For this purpose, we invite you to suggest discussion topics or questions by filling in your details [here](https://ellak.gr/phygital/){:target="_blank"} by Wendsday 9/12.

 
 &nbsp;
 

[Invitation Flyer (EL)]({{ "/assets/files/2020_12_01-EKDHLWSH-14-16-DEKEMBRIOU.pdf" | prepend: site.baseurl }})


 &nbsp;