---
layout: post
title:  "INTERACTIVE WORKSHOP - Makerspace: Creative Ideas & Communication Skills"
date:   2020-06-14 00:00:00 -0300
location: Korytsa, Albania
categories: news
featured: '/images/2020_06_14-interactive-workshop-makerspace_header.png'
excerpt: "HEC Foundation invites you on Saturday 18/07/2020 at the 1 st Interactive Workshop,
that brings together: creative & innovative ideas, communications skills, entrepreneurs
and young people."
---

# INTERACTIVE WORKSHOP - Makerspace: Creative Ideas & Communication Skills
“the digital SHAPE of cultural HERITAGE”


**HEC Foundation** invites you on Saturday 18/07/2020 at the 1st Interactive Workshop, that brings together: creative & innovative ideas, communications skills, entrepreneurs and young people.


The #KorytsaMakerspaces will give you access to free space and tools. Transform your ideas into reality with our project support!

In order to learn what a #Makerspace is, join us!

# Moderator, project staff:

#MarielaStefanllari

#NikolinDrabo

#XhulianaPapamihal

#JoanDeli

#ChristoforosChaskos


# Location

@YouthCentre Conference Hall, 11:00-16:00, Korçë-Albania


# Contact

Mariela Stefanllari - Project Manager

Email: hec.foundation@yahoo.com

Phone: +355 673133777



[Agenda]({{ "/images/2020_06_14-interactive-workshop-makerspace_Agenda.jpg" | prepend: site.baseurl }})

[Poster]({{ "/images/2020_06_14-interactive-workshop-makerspace_Poster.jpg" | prepend: site.baseurl }})

[1st Interactive Workshop Invitation]({{ "/assets/files/2020_06_14-1st_Interactive_Workshop_Invitation_18_Jul_2020.pdf" | prepend: site.baseurl }})
