---
layout: post
title:  "The Municipality of Northern Tzoumerka kindly invites you to the 'Wandering' Final Event of the Phygital project."
date:   2020-09-09 00:00:00 -0300
location: Northern Tzoumerka, Greece
categories: news
featured: '/images/2020_09_09-Wandering-Header.png'
excerpt: "The Municipality of Northern Tzoumerka kindly invites you to the 'Wandering' Final Event of the Phygital project. The event will take place on Monday 14 & Tuesday 15 of September in the wider area of the Municipality of Northern Tzoumerka."
---

The Municipality of Northern Tzoumerka kindly invites you to the 'Wandering' Final Event of the Phygital project. The event will take place on Monday 14 & Tuesday 15 of September in the wider area of the Municipality of Northern Tzoumerka.

# Location

Conference Hall "Kostas Krystallis", Sirrako, Northern Tzoumerka, Greece

Hotel "Orizontes" Pramanta, Northern Tzoumerka, Greece

Traditional Cafe "Platanos", Northern Tzoumerka, Greece

Ellinikon Tavern "Karabas", Northern Tzoumerka, Greece

# When

Sirrako: Monday, September 14 2020, 19:00

Pramanta: Tuesday, September 15 2020, 13:30

Plaisia: Tuesday, September 15 2020, 17:30

Elliniko: Tuesday, September 15 2020, 19:30

# Contact

Alexandros Pazaitis

Email: alex.pazaitis@gmail.com

Mobile: (+30) 6972916707
 
 &nbsp;
 
 &nbsp;

[Invitation Flyer (EL)]({{ "/assets/files/2020_09_09-invitation_el.pdf" | prepend: site.baseurl }})

[Invitation Flyer (EN)]({{ "/assets/files/2020_09_09-invitation_en.pdf" | prepend: site.baseurl }})
