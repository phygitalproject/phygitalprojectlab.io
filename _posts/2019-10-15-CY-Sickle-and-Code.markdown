---
layout: post
title:  "Sickle & Code"
date:   2019-10-15 00:00:00 -0300
location: Lakatamia, Cyprus
categories: news
featured: '/images/2019_10_15-stickie_and_code_header.jpg'
excerpt: "Phygital Project Exhibition. Co-organised by the University of Nicosia Research Foundation and Lakatamia Municipality. A programme conceptualised for Cyprus by Dr. Chrystalleni Loizidou within the scope of investigating contemporary movements towards a knowledge-sharing economy that reconsiders capitalist definitions of credit, labour and production."
---


# Sickle & Code
University of **Nicosia Research Foundation** and the **Lakatamia Municipality**

# Location

Museum of History and Cultural Heritage of Lakatamia
7 Agias Paraskevis, Lakatamia, 2311
[https://goo.gl/maps/suVx2PvRAiuTHYm69](https://goo.gl/maps/suVx2PvRAiuTHYm69)

# Opening
24 October 2019, 18:00-22:00
18:00 by the mayor of the Municipality of Lakatamia, Dr Photoula Hadjipapa
Performance during the opening by Elena Savvidou

# Artist Talks by Angelos Plessas and Bahar Noorizadeh
23 October 2019, 19:00
Unesco Amphitheater, University of Nicosia

# Opening hours
Wednesday 16:00 - 20:00, Friday 16:00 - 20:00, Saturday 15:00 - 19:00

# Exhibition duration
26 October 2019 - 15 February 2020

# Parallel programme of events
28 September 2019 - March 2020

# Participating artists
Maria Andreou, Raissa Angeli, Adonis Archontides, Helen Black & Yiannis Colakides, Zach Blas, Jenny Dunn, Nihaal Faizal, Veronika Georgiou, Olga Micioska, Bahar Noorizadeh, Angelo Plessas, Tabita Rezaire, Elena Savvidou, Emiddio Vasquez

# Phygital prototype team leaders
Ehsan Abdi, Odysseas Economides, Thrasos Nerantzis, Maria Toumazou, Kostas Tsangarides

# Design |Nico Stephou

#Curatorial team
Peter Eramian, Elena Parpa, Evanthia (Evi) Tselika

Sickle & Code is an international exhibition, part of Phygital, a programme conceptualised for Cyprus by Dr. Chrystalleni Loizidou within the scope of investigating contemporary movements towards a knowledge-sharing economy that reconsiders capitalist definitions of credit, labour and production. Phygital is currently implemented in Tzoumerka (Greece), Tirana (Albania) and Nicosia (Cyprus), involving the development of makerspaces with a focus on free and open source software driven by local communities.


As a title, Sickle & Code suggests a combination of tools: the sickle, an agricultural tool and once a resonant symbol of social revolutions; and the code, the language we develop as a tool in software programming. In contemporary debates coding links through movements,such as that of âfree and open-source softwareâ, with the demand, on a practical level, for open collaboration and unhindered re(distribution) of technologies and, on a theoretical level, with the social claim for freedom in access and in processes of making. Although the two tools reference distinct spheres of activity (the physical and the digital), they are both designed in order to shape the world we live in. As such, the Sickle & Code drives us to the core of the wider Phygital project, which builds on practices that move between the material and the digital, melding older forms of making and co-producing with current methods of fabrication and contemporary notions on the communal, collective and collaborative.


The Museum of History and Cultural Heritage of Lakatamia, in which the exhibition is being hosted, is housed in an old, traditionally-built residence that dates back to the British Colonial period (1922), when local materials and vernacular building methods were still applied in architecture across Cyprus. The museum is home to a wealth of objects that range from house furniture and utensils to farming tools, including a now disused sickle, and textile manufacturing equipment, which are meant to uncover the history of Lakatamia as an agricultural society, where life depended on community ties, knowledge sourced from nature and particular technological processes developed for survival. Acknowledging the specificity of such a context and the curatorial challenge of creating discursive links between current maker cultures and past methods of production and survival, the exhibitionâs display develops its own logic of classification and ordering of objects. Using a ubiquitous system of shelves, what already exists in the museum is reconfigured and recast under a new light in close conversation with what is brought in by the artists, prototype teams and overall programming of the exhibition.


More specifically, in Sickle & Code, Cypriot and international artists present works that reflect, confront and re-evaluate current models of producing under the conjunction of global digital commons of knowledge. Concurrently, the museum allows us to glimpse into everyday making and living practices of the recent past and becomes the conceptual testing ground for the five Cypriot Phygital pilot projects and prototypes, as the Lakatamia makerspace is being set up in the municipalityâs community centre. This is taking place within a wider emergence, in the last few years, of hackerspaces or makerspaces as community-led spaces, where free and open source software and hardware are utilized collaboratively by individuals.


The Lakatamia makerspace is being developed by the Municipality of Lakatamia in collaboration with the University of Nicosia Research Foundation and explores the melding of free and open technologies, social arts and maker cultures. Its context, seen in conjunction with the museum and the exhibition, allows us to reconsider our understanding of how we appreciate heritages of technologies, making, living and producing in-common(s) in times of digital and physical (Phygital) realities.The exhibition is accompanied by several parallel events that include makeathons, workshops, tours for schools and art schools, presentations, talks, performances and screenings. For updates and information on these events please visit: 

[https://thkioppalies.org/Projects](https://thkioppalies.org/Projects)

Sickle & Code is organized by the University of Nicosia Research Foundation as part of project Phygital (an Interreg V 2014-2020 BalkanMed, EU-funded programme) and co-organized in collaboration with the Fine Arts Programme, Department of Design and Multimedia and the Municipality of Lakatamia. The implementation partner is Thkio Ppalies. 


# Exhibition and public programme information
[https://thkioppalies.org/Projects](https://thkioppalies.org/Projects)
Contact info: info@thkioppalies.org

# Educational tours for schools and art schools
Athina Kyriacou (tel. 22364097 / 22364100)
athina.kyriacou@lakatamia.org.cy / tselika.e@unic.ac.cy


# Phygital info
[https://phygitalproject.eu/](https://phygitalproject.eu/)

# UNRF info
[https://www.unrf.ac.cy/](https://www.unrf.ac.cy/)

# Contact info

tselika.e@unic.ac.cy / +357 22842694



[facebook Poster]({{ "/images/2019_10_15-fb_poster_draft-02.jpg" | prepend: site.baseurl }})


[press release - en]({{ "/assets/files/2019_10_15-Sickle_and_Code_press_release_en.pdf" | prepend: site.baseurl }})


[press release - el]({{ "/assets/files/2019_10_15-Sickle_and_Code_press_release_el.pdf" | prepend: site.baseurl }})
