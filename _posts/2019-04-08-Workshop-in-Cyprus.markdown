---
layout: post
title:  "Workshop by University of Nicosia Research Foundation for the project Phygital"
date:   2019-04-08 00:00:00 -0300
location: Nicosia, Cyprus
categories: news
featured: '/images/2019_04_08_workshop_flyer_by_maria_hadjimichael.jpg'
excerpt: "Open Technologies: Their History, Laws, and Gender. A University of Nicosia Researh Foundation workshop for the project Phygital by Natalia- Rozalia Avlona"
---

**A**  **Workshop by**  **Maria Hadjimichael** University of Nicosia Research Foundation for the project Phygital


Narrating and Visualising the Commons in Environmental Justice Claims 
The potentialities of the digital-scape

# University of Nicosia Research Foundation


When the Republic of Cyprus put forward a bill in the House of Representatives, with the aim to introduce 'marine space' in the definition of real estate, a citizen initiative came forward against the amendment having the concept of 'the Commons' as a master frame of their campaign. This campaign was the first of many to follow. In the midst of a national narrative focusing on the need for societal sacrifices (for economic gains and national development), an environmental campaign brought forward claims linked to environmental and social justice. This resonated to part of the society who came forward to shout that 'society will not pay for Your crisis' and 'the Commons are not yours [the Government's] to sell'. Over the past four years, there were numerous bills, which came forward to facilitate the privatisation of public wealth (particularly on the coastline and the sea). At the same time, numerous big developments are beinglicensed and pushed forward. Areas which are meant to be under protectionstart being envisioned to become the courtyard for development enclaves / gated communities with latest developments regarding the future of the Akamas peninsula, one of the last areas on the island which has escaped the rapid cementation of the 1970s and 1980s, sparks a big wave of protests and interventions.
This workshop will follow the actions, discursive narratives and visual campaigning made in the post-crisis environmental campaigns in the Republic of Cyprus. Following an introduction around the concept of the Commons, with a particular focus on the ecological Commons and its links with the concept of Environmental Justice, an outline will follow on the actions linked to the above campaigns and their links to the Commons and Environmental Justice. The workshop which will ensue will include a presentation of discursive narratives but more so of visual images. The aim will be to discuss the linkages of the narratives and the visuals of these environmental campaigns with the concept of the Commons and Environmental Justice and their potential in becoming catalysts for movements of resistance. 


## About Maria Hadjimichael, PhD (maria.m.hadjimichael@gmail.com)

**Maria Hadjimichael** is a research fellow at the University of Cyprus focusing on the fields of political ecology (including urban), environmental politics and governance of the Commons with her main focus being the sea and the coastline.  How is the understanding of the sea and the coastal line as a 'Common' or 'Common Heritage' being affected by international agreements or national law, or even how is this being instrumentalised to expand the authority of the State? At the same time, Maria studies the theories of the Commons through her experiences as an activist in environmental as well as social issues.


## Phygital

**Phygital** is an Interreg V 2014-2020 BalkanMed, EU-funded programme being implemented in Greece, Albania and Cyprus and involves the development of makerspaces - one in each country - that will work with the local community. In Cyprus, the project's work is being carried out by the University of Nicosia Research Centre in collaboration with the Municipality of Lakatamia/hack66 and will focus on social art practices exploring the melding of open technology, art and design. The project operates on the basis of the "design global - manufacture local" model which introduces innovative organisational and business patterns allowing an unprecedented booming of communities engaged in do-it-yourself (DIY) activities. It wishes to support and enhance these local capacities for innovation and utilise the opportunities the decentralised modes of production can create. The Cyprus section of the project examines the importance of makerspace culture in the advancement of contemporary social art and design practices. It delves into the principles of open source projects, software-hardware freedom and bottom-up collaborative structures to explore ways they can be utilised - in line with social art/design practices - to address the needs of the local community. 

**PHYGITAL- Catalysing innovation and entrepreneurship unlocking the potential of emerging production and business models**
- Lead Partner-GFOSS/ Greek Free Open Source Software Society 
- Peer to Peer Alternatives - Greece (a.k.a. "P2P Lab")
- Lakatamia Municipality/ hack66- Cyprus
- Open Labs - Albania
- Municipality of North Tzoumerka- Greece
- University of Nicosia Research Foundation (UNRF)- Cyprus
- The National Center of Folklore Activities- Albania



**Workshop** Tuesday, 09 April 2019

18:00 - 21:00

**Location**

Graphic Design Studio (GDS3), Main Building

University of Nicosia

Nicosia

Cyprus


**Contact**

For more information you can contact

Evanthia Tselika

Email: tselika.e@unic.ac.cy

Phone: +35722842694

[![Workshop brochure]({{ "/images/2019_04_08_workshop_flyer_by_maria_hadjimichael.jpg" | prepend: site.baseurl }})]({{ "/images/2019_04_08_workshop_flyer_by_maria_hadjimichael.jpg" | prepend: site.baseurl }})


[press release - en]({{ "/assets/files/2019_04_08-Maria_Hadjimichael_press_release_en.pdf" | prepend: site.baseurl }})

[press release - el]({{ "/assets/files/2019_04_08-Maria_Hadjimichael_press_release_el.pdf" | prepend: site.baseurl }})
