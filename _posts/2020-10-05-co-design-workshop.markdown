---
layout: post
title:  "Co-design workshop, Lefkosia"
date:   2020-10-05 00:00:00 -0300
location: Lefkosia, Cyprus
categories: news
featured: '/images/2020_10_06-co-design-workshop.jpg'
excerpt: "The workshop is dedicated to collaborative design and has been set up in collaboration with members of the staff of the Cyprus Crafts Service and other stakeholders."
---

Organized and Coordinated by: **Christina Skarpari**

The workshop will take place at the premises of the Cyprus Handicrafts Service, Ministry of Trade, Industry and Energy, 191 Athalassas Avenue, Nicosia 2025.

**Tuesday, October 6, 9:00 - 13:00**

Places are limited and reservation required.

Contact Information: **info@christinaskarpari.org**

The workshop is dedicated to collaborative design and has been set up in collaboration with members of the staff of the Cyprus Crafts Service and other stakeholders. It is aimed at the general public and aims to develop a dialogue that has already begun with the selected participants of the service. The workshop negotiates the preservation and revitalization of traditional crafts in relation to new digital production methods (such as those presented in Makerspace spaces) and distributed manufacturing technologies (such as 3D printers or CNC machine tools).

 

The participants will have the opportunity to be guided in the various workshops of the Cyprus Crafts Service and to take part in a co-design process.

The second part of the workshop will focus on the crafts as they are negotiated by the craftsmen-trainers, the systemic operation of the Cyprus Crafts Service and how the new production methods can converse with these practices.

 

The workshop is open to people regardless of previous experience in the field of crafts.

 


A few words about the wider research and actions carried out in the framework of the Phygital project by Christina Skarpari.

The workshop takes place in the framework of the European Phygital Project and is the final action after a period of research formed with the involvement of the community of the Crafts Service of Cyprus in September 2020. This research is part of my doctoral study, which focuses in the involvement of people who practice Cypriot handicrafts that are in danger of extinction. In this context, I organize research activities related to experimentation and the preservation of aspects of cultural heritage. Therefore, the goal was to involve the trainers of the Cyprus Crafts Service.

 

The activities that I have formed, aim at the methodological development of a series of initiatives that focus on the involvement of communities in terms of crafts that are in danger of disappearing and in the places of creation of "makerspaces". Activities include: interviews, mapping exercises, and a workshop at the newly established Makerspace Lakatamia, a focus group that also formed the framework of the collaborative design workshop that is open to the public with teachers and other members of the local community.

 

This participatory research and workshop is conducted in the framework of my collaboration with the Research Foundation of the University of Nicosia, where I will have the opportunity to develop a methodology that characterizes my wider research. The program has been developed in collaboration with Dr. Evi Tselika of the University of Nicosia and the supervisors of my doctorate, Dr. Matt Malpass and Dr. Peter Hall, Central Saint Martins, University of the Arts London and Dr. Beth Cullens, of Westminster University.

# A few words about Phygital

Phygital is a program of Interreg V 2014-2020 BalkanMed, which is funded by the European Union and includes actions in Greece, Albania and Cyprus. The project will create makerspaces - one in each country - that will work with the local community. In Cyprus, the activities of the program are carried out by the Research Program of the University of Nicosia in collaboration with the Municipality of Lakatamia and focus on social art practices exploring the combination of open technology, art and design. The Program operates on the basis of the ‘design global - manufacture local’ model which introduces innovative organizational and business standards allowing an unprecedented development of the teams that create autonomously at the ‘do-it-yourself (DIY)’ level. Its aim is to support and strengthen, at the local level, the possibilities for innovation and to take advantage of the opportunities that are formed through the decentralized way of production. The Cyprus branch of the project examines the importance of makerspaces culture for the promotion of contemporary social art and design practices.


# Location

The workshop will take place at the premises of the Cyprus Handicrafts Service, Ministry of Trade, Industry and Energy, 191 Athalassas Avenue, Nicosia 2025.

# When

Tuesday, October 6, 9:00 - 13:00

Places are limited and reservation required.

# Contact

Email: info@christinaskarpari.org
 


[Invitation Flyer (EL)]({{ "/assets/images/2020_10_06_Invitation_flyer.jpg" | prepend: site.baseurl }})
