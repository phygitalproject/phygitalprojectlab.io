---
layout: page
title: Privacy policy
type: privacy
permalink: /privacy/
---

The Phygital project is committed to the protection of personal data, in accordance with the [General Data Protection Rules](http://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32016R0679). Your personal data will thus be processed in conformity with the above-mentioned Regulation.

Should you have any queries about the processing of your personal data, both regarding access to data and rectification of these data, you are invited to send your request to info[at]eellak.gr.

You can browse through Phygital website without giving any information. 

##  Cookies

### What are cookies

As is common practice with almost all professional websites this site uses cookies, which are tiny files that are downloaded to your computer, to improve your experience.  For more general information on cookies see the [Wikipedia article on HTTP Cookies...](http://en.wikipedia.org/wiki/HTTP_cookie) 

## Disabling cookies

You can prevent the setting of cookies by adjusting the settings on your browser (see your browser Help for how to do this). Be aware that disabling cookies will affect the functionality of this and many other websites that you visit. Disabling cookies will usually result in also disabling certain functionality and features of this site. Therefore it is recommended that you do not disable cookies. 

### Third party cookies

In some special cases we also use cookies provided by trusted third parties.
This site uses Google Analytics which is one of the most widespread and trusted analytics solution on the web for helping us to understand how you use the site and ways that we can improve your experience. These cookies may track things such as how long you spend on the site and the pages that you visit so we can continue to produce engaging content.
For more information on Google Analytics cookies, see the official [Google Analytics page](https://developers.google.com/analytics/resources/concepts/gaConceptsCookies).

## Disclaimer

Phygital project maintains this website to enhance public access to information about its initiatives. Our goal is to keep this information timely and accurate. If errors are brought to our attention, we will try to correct them. However, Interreg Phygital project  no responsibility or liability whatsoever with regard to the information on this site.

### This information is:

* of a general nature only and is not intended to address the specific circumstances of any particular individual or entity;
* not necessarily comprehensive, complete, accurate or up to date;
* sometimes linked to external sites over which Phygital Project has no control and for which Phygital Project assumes no responsibility;
* not professional nor provides legal advice (if you need specific advice, you should always consult a suitably qualified professional).

Please note that it cannot be guaranteed that a document available online reproduces exactly an officially adopted text. Only the printed text, signed by the appropriate authorities, is deemed authentic.

It is our goal to minimise disruption caused by technical errors. However some data or information on our site may have been created or structured in files or formats that are not error-free and we cannot guarantee that our service will not be interrupted or otherwise affected by such problems. Phygital Project accepts no responsibility with regard to such problems incurred as a result of using this site or any linked external sites.
