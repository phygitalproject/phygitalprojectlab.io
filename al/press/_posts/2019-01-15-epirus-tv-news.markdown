---
layout: post
langUrl: /al
title:  "Artikull në website-in epirus-tv-news.gr për Workshop-in Informativ të projektit PHYGITAL"
date:   2019-01-15 12:00:00 -0300
location: Ioannina, Greece
category: al-press
---

Artikulli i publikuar në website-in ['epirus-tv-news'](http://www.epirus-tv-news.gr/) i kushtohet organizimit të një Workshop-i i cili u zhvillua nga Bashkia e Xumerkës së Veriut si pjesë e strategjisë së komunikimit dhe të drejtës së informimit të publikut në lidhje me kontributin e Bashkisë në implementimin e projektit Phygital. 

[Article link](http://www.epirus-tv-news.gr/2019/01/phygital.html)