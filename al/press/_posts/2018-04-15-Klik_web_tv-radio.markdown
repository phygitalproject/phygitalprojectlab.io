---
layout: post
langUrl: /al
title:  "Artikulli në Klik TV apër teknologjitë e hapura për komunitetin në Xumerkë"
date:   2018-04-15 12:00:00 -0300
location: Thessaloniki, Greece
category: al-press
---

Artikulli i publikuar në [Kliktv.gr](https://kliktv.gr) i është kushtuar zhvillimit dhe përhapjes së teknologjive të hapura për komunitetin në Xumerkë. 

[ Klik tv link (in greek)](https://kliktv.gr/%cf%84%ce%b6%ce%bf%cf%85%ce%bc%ce%ad%cf%81%ce%ba%ce%b1-%ce%b7-%ce%b3%ce%ad%ce%bd%ce%bd%ce%b7%cf%83%ce%b7-%ce%bc%ce%b9%ce%b1%cf%82-%ce%ba%ce%bf%ce%b9%ce%bd%cf%8c%cf%84%ce%b7%cf%84%ce%b1%cf%82-%ce%b1/)