---
layout: post
langUrl: /al
title:  "Artikull në portalin typos-i.gr për takimin e Tzoumakers në Kalenxi"
date:   2018-05-18 12:00:00 -0300
location: Tzoumerka, Greece
category: al-press
---

Artikulli i publikuar në portalin ['typos-i.gr](https://typos-i.gr/) ka të bëjë me takimin e parë Tzumakers në Kalenxi. 

[Article link](https://typos-i.gr/article/kai-onoma-aytoy-tzoumakers)