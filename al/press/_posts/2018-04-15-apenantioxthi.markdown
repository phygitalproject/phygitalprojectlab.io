---
layout: post
langUrl: /al
title:  "Artikulli në gazetën virtuale 'Apenanti Oxthi' të teknologjive të hapura në komunitetin e Xumerkës"
date:   2018-05-14 12:00:00 -0300
location: Tzoumerka, Greece
category: al-press
---

Artikulli i portalit ['Apenanti Oxthi'](https://www.apenantioxthi.com) është i dedikuar në emergjencën dhe zhvillimin e teknologjive të hapura të komunitetit në Xumerkë. 

[Article link](https://www.apenantioxthi.com/2018/04/tzoumerka-h-gennhsh-mias-koinothtas-anoixtwn-texnologiwn.html)