---
layout: page
langUrl: /al
title: Politika e privatësisë
type: privacy
permalink: /al/privacy/
---

Projekti Phygital është i përkushtuar në garantimin e mbrojtjes së të dhënave personale, në përputhje me [Kondicionet e Përgjithshme të Privatësisë](http://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32016R0679). Të dhënat tuaja private do të proçedohen konform regulacionit të sipërpërmendur.

Në rast se keni pyetje në lidhje me mënyrën sesi procesohen të dhënat tuaja personale, qoftë në raport me marrjen e tyre dhe përdorimin e mëtejshëm, ju ftojmë të dërgoni kërkesën tuaj në adresën elektronike info[at]eellak.gr.

Ju mund të navigoni në website-in e Phygital-it, pa dhënë asnjë informacion.


##  Cookies

### Çfarë janë cookies-at?

Ashtu siç është praktikë e zakonshme në pothuajse të gjitha website-t profesionale edhe kjo faqe përdor cookies, të cilat janë dosje me përmasa tejet të vogla që shkarkohen në kompjuterin tuaj për ta bërë eksperiencën e navigimit më të lehtë. Për më tepër informacione të përgjithshme për cookies-at, shihni artikullin e  [Wikipedia-s për HTTP Cookies...](http://en.wikipedia.org/wiki/HTTP_cookie) 

## Çaktivizimi i Cookies-ave

Ju mund të parandaloni shkarkimin e cookies-ave, duke axhustuar të dhënat në browser-in tuaj (konsultohuni me sektorin “Help” në browser për të parë sesi mund të ndërmerrni këtë veprim). Ju bëjmë me dije se çaktivizimi i cookies do të ndikojë në funksionalitetin e shumë faqeve të cilat do të vizitoni. Çaktivizimi i tyre do të shkaktojë dhe çaktivizimin e disa tiparave të funksionalitetit në këtë faqe elektronike. Për pasojë, ju rekomandojmë që të mos i çaktivizoni ato. 

### Cookie-t e palëve të treta

Në disa raste të veçanta ne përdorim cookies të furnizuara nga palë të treta, tek të cilat kemi besim. Ky site përdor Google Analytics, e cila është një nga zgjdhjet analitike më të përhapura dhe të besuara në internet, e cila na mundëson të kuptojmë sesi ju e përdorin faqen tonë dhe sesi mund të përmirësojmë eksperiencën tuaj në të. Këto cookies mund të gjurmojnë sa gjatë rrini në faqen tonë dhe mënyrën sesi angazhoheni me përmbajtjen e faqes. Për më shumë informacione në lidhje me cookie-t e Google Analytics, shihni faqen zyrtare të [Google Analytics page](https://developers.google.com/analytics/resources/concepts/gaConceptsCookies).

## Njoftim

Projekti Phygital mirëmban website-in për të informuar publikun e gjërë në lidhje me aktivitetet e tij. Qëllimi ynë është që ky informacion të jepet në kohë dhe të jetë i saktë. Në rast se gabime të ndryshme vijnë në vemendjen tonë, ne do të përpiqemi t’i korrektojmë. Megjithatë, Projekti Phygital nuk mban asnjë përgjegjësi, ligjore ose të çfarëdolloj forme në lidhje me përmbajtjen e këtij website-i. 

### Ky informacion është:

* I një natyre të përgjithshme dhe ka për qëllim të vetëm adresimin e rrethanave specifike për çdo individ dhe ent;
* nuk eshte domosdoshmërisht i kuptueshëm, i plotë, i saktë ose i përditësuar;
* disa herë projekti Phygital ka link-e tek site të jashtëm për përmbajtjen e të cilave nuk ka asnjë kontroll dhe nuk mban asnjë përgjegjësi;
* faqja nuk ofron këshilla profesionale ose ligjore (në rast se u duhen këshilla specifike, ju duhet të kontaktoni një profesionit të kualifikuar)

Ju lutem, mbani parasysh se nuk mund të garantohet që një dokument, i cili është i disponueshëm online, mund të riprodhohet në një tekst të njohur zyrtarisht. Vetëm teksti i printuar, i firmosur nga autoritetet përkatëse mund të konsiderohet si autentik. 

Është qëllimi ynë që të minimizojmë ndërprerjet për shkak të probleme teknike. Megjithatë, një pjesë e të dhënave ose e informacioneve në faqen tonë mund të krijohen ose të klasifikohen në formate të cilat nuk janë gjithmonë jo-difektoze dhe nuk mund të garantojmë se shërbimi ynë do të funksionojë pa ndërprerje ose i pandikuar nga këto probleme. Projekti Phygital nuk pranon asnjë përgjegjësi në lidhje me problemet që mund të krijohen si pasojë e përdorimit të këtij site-i ose ndonjë nga lidhjet e jashtme të tij. 
