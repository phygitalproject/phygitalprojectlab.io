---
layout: post
langUrl: /al
title:  "Takimi i parë i hapur i Focus Group-it në Kalenxi"
date:   2018-04-26 12:00:00 -0300
location: Kalentzi, Greece
categories: newsal
featured: '/images/Poster_FocusGroup1_featured.png'
---

Një takim i hapur i Focus Group-it do të zhvillohet në qendrën kulturore të Bashkisë së Kalenxit në datën 26 Prill

Çështjet që do të diskutohen përgjatë takimit do të jenë Teknologjitë e Hapura në Sektorin Primar dhe eksperiancat e fituara prej vizitës në shtabin e ekipit francez ["L'Atelier Paysan"](https://www.latelierpaysan.org/) si dhe ato të marra nga konstruksioni në vazhdim i hapësirës së krijimit Makerspace, e cila është e sponsorizuar nga [Projekti Phygital](http://www.interreg-balkanmed.eu/approved-project/29/).

[Poster (in greek)]({{ "/images/Poster_FocusGroup1.png" | prepend: site.baseurl }})

[Invitation (in greek)]({{ "/assets/files/Invitation_FocusGroup1.pdf" | prepend: site.baseurl }})

[Press release (in greek)]({{ "/assets/files/D3.2.2.prePressRelease_FocusGroup1.pdf" | prepend: site.baseurl }})

[Agenda (in greek)]({{ "/assets/files/Agenda_FocusGrou1.pdf" | prepend: site.baseurl }})
