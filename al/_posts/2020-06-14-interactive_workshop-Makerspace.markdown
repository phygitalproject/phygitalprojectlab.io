---
layout: post
langUrl: /al
title:  "WORKSHOP INTERAKTIV - Makerspace: Ide krijuese & Aftësi Komunikimi"
date:   2020-06-14 00:00:00 -0300
location: Korytsa, Albania
categories: newsal
featured: '/images/2020_06_14-interactive-workshop-makerspace_header.png'
excerpt: "Fondacioni HEC ju fton ditë e shtunë, data 18/07/2020 në Workshop-in e parë interaktiv që sjell së bashku: ide krijuese & inovative, aftësi komunimi, sipërmarrës dhe të rinj."
---

# WORKSHOP INTERAKTIV - Makerspace: Ide Krijuese & Aftësi Komunikuese
“Forma Dixhitale e Trashëgimnisë Kulturore”


**Fondacioni HEC** ju fton ditë e shtunë, data 18/07/2020 në Workshop-in e parë interaktiv që sjell së bashku: ide krijuese & inovative, aftësi komunimi, sipërmarrës dhe të rinj.


#KorytsaMakerspaces do ju ofrojë akses të lirë në hapësirën e vet dhe në përdorimin e pajisjeve. Transformoni idetë tuaja në realitet, me mbështetjen e projektit tonë!

Në mënyrë për të mësuar se çfarë është një #Makerspace na u bashko!

# Moderator, stafi i projektit:

#MarielaStefanllari

#BelinaBudini

#NikolinDrabo

#XhulianaPapamihal

#JoanDeli

#ChristoforosChaskos


# Vendndodhja

@Salla e Konferencave e Qendrës Rinore, ora 11:00-16:00, Korçë-Shqipëri


# Kontakt

Mariela Stefanllari - Menaxher i Projektit

Email: hec.foundation@yahoo.com

Phone: +355 673133777



[Agenda]({{ "/images/2020_06_14-interactive-workshop-makerspace_Agenda.jpg" | prepend: site.baseurl }})

[Poster]({{ "/images/2020_06_14-interactive-workshop-makerspace_Poster.jpg" | prepend: site.baseurl }})

[1st Interactive Workshop Invitation]({{ "/assets/files/2020_06_14-1st_Interactive_Workshop_Invitation_18_Jul_2020.pdf" | prepend: site.baseurl }})
