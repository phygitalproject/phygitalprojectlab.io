---
layout: post
langUrl: /al
title:  "Eventi i Parë Makeathon, Korçë"
date:   2020-02-05 00:00:00 -0300
location: Korçë-Albania
categories: newsal
featured: '/images/2020_02_05_1st_Makeathon_Invitation_header.png'
excerpt: "Fondacioni HEC ju fton ditën e shtunë, datë 15/02/2020 në eventin e parë Makeathon, një event interaktiv dhe krijues që sjell së bashku: ide, aftësi, makineri elektronike, sipërmarrës dhe të rinj"
---

# Eventi i Parë Makeathon, Korçë
**Fondacioni HEC** ju fton ditë e shtunë, datë 15/02/2020 në eventin e Parë Makeathon, një event interaktiv dhe krijues që sjell së bashku: ide, aftësi, makineri elektronike, sipërmarrës dhe të rinj.

#Makerspace-i i Korçës, do u ofrojë akses të lirë në hapësirën e vet, si dhe për përdorimin e mjeteve. Shndërrojini ëndrrat tuaja në realitet me mbështetjen e projektit tonë!
Për të mësuar se çfarë është një #Makerspace na u bashko!


# Moderatori, stafi i projektit dhe folësit e ftuar:

Moderatori, stafi i projektit dhe folësit e ftuar:
\#MarielaStefanllari
\#NikolinDrabo
\#XhulianaPapamihal
\#JoanDeli
\#EleinaQirici

# Kur

E shtunë, data 15 Shkurt 2020, 11:00-16:00

# Vendndodhja 

Korçë-Shqipëri

# Kontakt

Kontakt
Nikolin Drabo - Kordinator i Projektit
Email: drabonikolin@gmail.com
Phone: +355 683338665


Mariela Stefanllari - Menaxheri i Projektit
Email: hec.foundation@yahoo.com
Phone: +355 673133777


# INVITATION
[INVITATION]({{ "/assets/files/2020_02_05_1st_Makeathon_Invitation_15February_2020.pdf" | prepend: site.baseurl }})

# AGENDA
[AGENDA]({{ "/assets/files/2020_02_05_1st_Makeathon_Agenda_15_February_2020.pdf" | prepend: site.baseurl }})


