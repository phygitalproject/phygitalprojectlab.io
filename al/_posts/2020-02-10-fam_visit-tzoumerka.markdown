---
layout: post
langUrl: /al
title:  "Vizitë familjarizuese për blogger-at e udhëtimeve dhe gazetarët në Xumerkën e Veriut"
date:   2020-02-10 00:00:00 -0300
location: Northern Tzoumerka, Greece
categories: newsal
featured: '/images/2020_02_05_1st_Makeathon_Invitation_header.png'
excerpt: "Bashkia e Xumerkës së Veriut, ju fton me kënaqësi në vizitën për familjarizim të blogger-ave të udhëtimeve dhe gazetarëve në Xumerkën e Veriut, e cila do të zhvillohet nga data 27 Mars deri në datën 29 Mars 2020."
---

# Vizitë familjarizuese

**Bashkia e Xumerkës së Veriut** ju fton me kënaqësi në vizitën për familjarizim të blogger-ave të udhëtimeve dhe gazetarëve në Xumerkën e Veriut, e cila do të zhvillohet nga data 27 Mars deri në datën 29 Mars 2020. Ky udhëtim familjarizues, ka për qëllim nënvizimin e vlerave të trashëgimnisë kulturore dhe natyrore të zonës së Xumerkës të Veriut, nëpërmjet publikimeve dhe postimeve të targetuara në rrjetet sociale dhe më gjerë në media. Vëmendje e veçantë do i kushtohet aktiviteteve të projektit Phygital dhe në veçanti punës së makerspace-it rural “Tzoumakers” e cila ndodhet në Kalenxi të bashkisë të Xumerkës së Veriut në Greqi.


# Kur

27-29 Mars 2020

# Vendndodhja 

Xumerka e Veriut, Greqi.


Afati i fundit për dorëzimin e aplikimeve është dita e mërkurë, **25 Shkurt 2020**


# INVITATION
[INVITATION]({{ "/assets/files/2020_02_10-fam_visit-tzoumerka-prosklhsh.pdf" | prepend: site.baseurl }})

# REGISTRATION FORM
[REGISTRATION FORM]({{ "/assets/files/2020_02_10-fam_visit-tzoumerka-aithsh.pdf" | prepend: site.baseurl }})


