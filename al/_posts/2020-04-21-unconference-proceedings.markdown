---
layout: post
langUrl: /al
title:  "Proçes verbali për takimin interaktiv të 30 Majit - 1 Qershorit 2019"
date:   2020-04-21 00:00:00 -0300
location: Nicosia, Cyprus
categories: newsal
featured: '/images/2020-04-21-Unconference_Proceedings_Phygital-header_02.jpg'
excerpt: "Më shumë se 100 njerëz morën pjesë në takimin interaktiv për teknologjitë e lira, artin dhe pronën e përbashkët, pjesë e projektit Phygital, i cili u zhvillua nga dita e enjte 30 Maj dhe deri të shtunën, 1 Qeshor 2019 në Universitetin e Nikosisë në Qipro"
---


Një **takim interaktiv** në lidhje me artin, dizajnin, teknologjinë, krijimin, qytetet dhe komunitet e tyre e organizuar nga Fondacioni Kërkimor i Universitetit të Nikosisë, në kuadër të projektit Phygital. Më shumë se 100 njerëz morën pjesë në takimin interaktiv për teknologjitë e lira, artin dhe pronën e përbashkët, pjesë e projektit Phygital, i cili u zhvillua nga dita e enjte 30 Maj dhe deri të shtunën, 1 Qeshor 2019 në Universitetin e Nikosisë në Qipro.

[https://phygitalproject.eu/news/2019/05/14/Unconference-about-Art-Design-Technology-Making-Cities-and-their-Communities.html](https://phygitalproject.eu/news/2019/05/14/Unconference-about-Art-Design-Technology-Making-Cities-and-their-Communities.html)


[PROCEEDINGS]({{ "/assets/files/2020-04-21-Unconference_Proceedings_Phygital.pdf" | prepend: site.baseurl }})
