---
layout: post
langUrl: /al
title:  "Rezultatet e takimit të parë të hapur të Focus Group-it në Kalenxi"
date:   2018-04-27 12:00:00 -0300
location: Kalentzi, Greece
categories: newsal
featured: '/images/Poster_FocusGroup1_featured.png'
---

Dokumentat informuese për takimin e parë të hapur të Focus Group-it, i cili u zhvillua ditën e djeshme në Kalenxi, në bashkëpunim me [P2P Lab](http://www.p2plab.gr/en/) mund të gjenden në link-un e mëposhtëm.

[debriefing (in greek)]({{ "/assets/files/D3.2.2.postPressRelease_FocusGroup1.pdf" | prepend: site.baseurl }})
