---
layout: post
langUrl: /al
title:  "Rezultat e takimit të dytë të hapur Focus Group-it në Kalenxi"
date:   2018-05-15 12:00:00 -0300
location: Kalentzi, Greece
categories: newsal
featured: '/images/Poster_FocusGroup2_featured.png'
---

Dokumenti informues për takimin e dytë të hapur të Focus Group-it, i cili u zhvillua ditën e djeshme në Kalenxi, në bashkëpunim me [P2P Lab](http://www.p2plab.gr/en/) mund të gjenden në linkun e mëposhtëm.

[debriefing (in greek)]({{ "/assets/files/D3.2.2.postPressRelease_FocusGroup2.pdf" | prepend: site.baseurl }})
