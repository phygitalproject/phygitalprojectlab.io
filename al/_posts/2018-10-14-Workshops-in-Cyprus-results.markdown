---
layout: post
langUrl: /al
title:  "Rezultatet e workshop-eve të zhvilluara nga Fondacioni Kërkimor i Universitetit të Nikosisë"
date:   2018-10-14 12:00:00 -0300
location: Nicosia, Cyprus
category: newsal
featured: '/images/1st_UNRF_workshop.png'
---

Materiali informues dhe burimet informative nga workshop-et e zhvilluara nga Fondacioni Kërkimor i Universitetit të Nikosisë në 11 dhe 13 Tetor 2018, në lidhje me debatet bashkëkohore për hackespaces dhe potenciali i tyre për të qenë hapësira ndryshimi social dhe kreativiteti, janë të disponueshme në link-et e mëposhtëm.

[Video link](https://youtu.be/oxfBa0NMrIEf)

[Debriefing document]({{ "/assets/files/1st_UNRF_workshop.pdf" | prepend: site.baseurl }})
