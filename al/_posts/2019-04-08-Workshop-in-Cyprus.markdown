---
layout: post
langUrl: /al
title:  "Workshop i organizuar nga Fondacioni Kërkimor i Universitetit të Nikosisë për projektin Phygital"
date:   2019-04-08 00:00:00 -0300
location: Nicosia, Cyprus
categories: newsal
featured: '/images/2019_04_08_workshop_flyer_by_maria_hadjimichael.jpg'
excerpt: "Teknologjitë e Hapura: Historia e tyre, Ligjet dhe Gjinitë. Një workshop i organizuar nga Fondacioni Kërkimor i Universitetit të Nikosiosë për projektin Phygital nga Natalia- Rozalia Avlona"
---

**Një**  **Workshop nga**  **Maria Haxhimihal** Fondacioni Kërkimor i Universitetit të Nikosisë për projektin Phygital


Narracioni dhe vizualizimi i Bashkëpunimit në çështjet e Drejtësisë Mjedisore
Potencialet e hapësirës dixhitale


# Fondacioni kërkimor i Universitetit të Nikosisë


Kur Republika e Qipros parashtroi një projekt-ligj për t'u shqyrtuar nga Parlamenti, me qëllim caktimin e rolit të marinës në përcaktimin e pronave të paluajtshme, lindi një iniciativë qytetare, e cila propozoi një amendament që vendoste konceptin e pronësisë si vlerë themelore të fushatës së tyre. Kjo fushatë ishte e para e shumë të tjerave. Në mes të një narrative kombëtare që fokusohej tek nevoja e sakrificave shoqërore (për rritje ekonomike dhe zhvillim kombëtar), një fushatë mjedisore lidhi çështjen e mjedisit me drejtësinë sociale. Kjo rezonoi me një pjesë të shoqërisë që doli përpara me slloganin 'shoqëria do të paguajë për krizën tuaj' dhe 'pronat publike nuk janë tuajat [të qeverisë] për t'u shitur'. Përgjatë katër viteve të fundit ka pasur një numër të madh projekt-ligjesh, të propozuara për të ndihmuar me privatizimin e pasurisë publike (veçanërisht në vijën bregdetare). Njëkohësisht, një numër i madh zhvillimesh kanë ndodhur prej atij momenti. Zona të cilat supozohet të jenë të mbrojtura fillojnë të konsiderohen për t'u shndërruar në zona zhvillimi, ose zona rezidenciale të mbyllura. Ngjarjet e fundit në lidhje me të ardhmen e peninsulës së Akamasë, një nga zonat e fundit të cilat i kanë shpëtuar betonizimit të viteve 1970 dhe '80, ngjalli një valë të madhe protestash dhe ndërhyrjesh.
Ky workshop do të ndjekë veprimet, narrativat dhe fushatat vizuale, të cilat u zhvilluan pas krizës mjedisore në Republikën e Qipros. Pas një prezantimi të konceptit të pronës së përbashkët, me një fokus të veçantë në pronën e përbashkët ekologjike dhe lidhjen me konceptin e Drejtësisë Mjedisore. Workshop-i që do të pasojë do të përfshijë një prezantim të narrativës dhe retorikës publike, por edhe të imazheve të cilat u përdorën përgjatë fushatave. Qëllimi është të diskutohen lidhjet mes narrativës dhe imazheve të këtyre fushatave me Drejtësinë Mjedisore dhe potencialin për t'u bërë katalizator për lëvizjet e rezistencës.
 


## Rreth Maria Hadjimichael, PhD (maria.m.hadjimichael@gmail.com)

**Maria Hadjimichael** është një studiuese në Universitetin e Qipros e fokusuar në fushën e ekologjisë politike (duke përfshirë dhe ekologjinë urbane), politikave të mjedisit dhe administrimit të pronave të përbashkëta me fokus kryesor tek deti dhe vije bregdetare. Si po preken konceptet e pronësisë publike të detit dhe vijës bregdetare nga marrëveshjet ndërkombëtare ose ligji ndërkombëtar, ose si kjo po instrumentalizohet për të rritur autoritetin e shtetit. Njëkohësisht, Maria studion teoritë e pronës së përbashkët, nëpërmjet eksperiencave të saj si aktiviste në mbrojtje të mjedisit dhe aktive në çështjet sociale.


## Phygital

**Phygital** është një program Interreg V 2014-2020 i BalkanMed, i financuar nga BE, i cili po implementohet në Greqi, Shqipëri dhe Qipro dhe përfshin zhvillimin e Makerspace-ave, nga një për çdo vend pjesëmarrës, që do të jenë në përdorim nga komunitetet lokale. Në Qipro, puna e projektit po bëhet nga Fondacioni Kërkimor i Universitetit të Nikosisë, në bashkëpunim me Bashkinë e Lakatamisë dhe hack66, duke u fokusuar në praktikat e artit social dhe tek eksplorimi i teknlogjisë së hapur, artit dhe dizajnit. Projekti operon me moton dhe modelin "dizenjo globalisht, prodho lokalisht" dhe ofron metoda organizimi inovative dhe modele biznesi që do të mundësojnë një shtim të papreçedentë të njerëzve nga komuniteti që angazhohen në krijimtari të bërë vetë. Programi synon të mbështesë dhe të rrisë kapacitet lokale për inovacion dhe të shfrytëzimin e oportuniteteve që ofrojnë format e deçentralizuara të prodhimit. Seksioni qipriot i projektit ekzaminon rëndësinë e kulturës së Makerspacave për zhvillimin e artit social bashkëkohor dhe praktikat e dezajnit. Ky seksion thellohet në parimet e projekteve me burime të hapur, lirinë e software-it dhe hardware-it si dhe strukturat e bashkëpunimit nga poshtë-lart për të ekspoluar mënyrat sesi ato mund të përdoren për të adresuar nevojat e komuniteteve lokale. 

**PHYGITAL- Katalizojmë inovacionin dhe sipërmarrjen duke çliruar potencialin e produkteve dhe modeleve të biznesit në zhvillim**
- Partnerin Kryesor-GFOSS/ Shoqëria Greke e Burimeve Software të Hapura 
- Alternativa Shoku-Shokut - Greqi (e njohur ndryshe si "P2P Lab")
- Bashkia e Lakatamisë/hack66- Qipro
- Open Labs - Shqipëri
- Bashkia e Xumerkës së Veriut- Greqi
- Fondacioni Kërkimor i Universitetit të Nikosisë- Qipro
- Qendra Kombëtare për Aktivitetet Folklorike- Shqipëri



**Workshop** E martë, 09 Prill 2019

18:00 - 21:00

**Vendndodhja**

Graphic Design Studio (GDS3), Ndërtesa Kryesore

Universiteti i Nikosisë

Nikosi

Qipro


**Kontakt**

Për më shumë informacione mund të kontaktoni

Evanthia Tselika

Email: tselika.e@unic.ac.cy

Phone: +35722842694

[![Workshop brochure]({{ "/images/2019_04_08_workshop_flyer_by_maria_hadjimichael.jpg" | prepend: site.baseurl }})]({{ "/images/2019_04_08_workshop_flyer_by_maria_hadjimichael.jpg" | prepend: site.baseurl }})


[press release - en]({{ "/assets/files/2019_04_08-Maria_Hadjimichael_press_release_en.pdf" | prepend: site.baseurl }})

[press release - el]({{ "/assets/files/2019_04_08-Maria_Hadjimichael_press_release_el.pdf" | prepend: site.baseurl }})
