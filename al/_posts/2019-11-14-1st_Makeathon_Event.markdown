---
layout: post
langUrl: /al
title:  "Eventi i Parë Makeathon"
date:   2019-11-14 00:00:00 -0300
location: Lakatamia, Cyprus
categories: newsal
featured: '/images/2019_11_14-1st_Makeathon_event.png'
excerpt: "Bashkia e Lakatamisë dhe Fondacioni Kërkimor i Universitetit të Nikosisë ju fton të shtunën, më datë 16/11/2019 në eventin e parë Makeathon."
---

# Eventi i Parë Makeathon
**Bashkia e Lakatamisë** dhe **Fondacioni Kërkimor i Universitetit të Nikosisë** ju fton të Shtunën, më datë 16/11/2019 në eventin e parë Makeathon. 

Mësoni sesi nëpërmjet hapësirave "Makerspace" mund të keni akses tek përdorimi i lirë i pajisjeve. Shndërrojini idetë tuaja në realitet! 

# Çfarë është një makerspace?

Makerspace-at se hackerspace-at janë laboratorë që operohen kolektivisht, anëtarët e të cilave kanë aftësinë të përdorin mjetet dhe pajisjet (si p.sh printerat 3D, prerëset me lazet, CNC-itë, etj.), akses të cilin nuk do e kishin në kushte normale. Në makerspace-at ose hackerspace-at, dijet dhe aftësitë transmetohen dhe shpërndahen. Projektet e ndryshme të cilat zhvillohen në këto fusha janë zakonisht rezultat i bashkëpunimit mes pjesëmarrësve në komunitet ose anëtarëve të makerspace-it, të cilët janë një komunitet solidariteti për krijuesit.

# Si munnd të marrësh pjesë?

Hidhi një sy, merr pjesë dhe informohu në lidhje me aktivitetet e Makerspace-it në datën 16/11/2019 midis orës 2:00 pm - 6:00 pm të shtunën dhe të dielën. Pranimi për publikun është i lirë

# Kur

Të shtunën, datë 16 Nëntor 2019, ora 14:00-18:00

# Vendndodhja 

Qendra multifunksionale e Lakatamisë

# Kontakt

Athina Kyriacou

Email: athina.kyriacou@lakatamia.org.cy
Tel: 22364046 /97

Marios Podinas - Koordinatori i Projektit

Email: marios@gnous.com
Tel: 99842995







# PHYGITAL A3 POSTER ONLINE
[![PHYGITAL A3 POSTER ONLINE]({{ "/images/2019_11_14-1st_Makeathon_event-POSTER-ONLINE.png" | prepend: site.baseurl }})]({{ "/images/2019_11_14-1st_Makeathon_event-POSTER-ONLINE.png" | prepend: site.baseurl }})

# PHYGITAL NEWSLETTER (EL)
[![PHYGITAL NEWSLETTER (EL)]({{ "/images/2019_11_14-1st_Makeathon-NEWSLETTER_1.png" | prepend: site.baseurl }})]({{ "/images/2019_11_14-1st_Makeathon-NEWSLETTER_1.png" | prepend: site.baseurl }})

# PHYGITAL NEWSLETTER (EN)
[![PHYGITAL NEWSLETTER (EN)]({{ "/images/2019_11_14-1st_Makeathon-NEWSLETTER_2.png" | prepend: site.baseurl }})]({{ "/images/2019_11_14-1st_Makeathon-NEWSLETTER_2.png" | prepend: site.baseurl }})
