---
layout: post
langUrl: /al
title:  "Axhenda e Eventit Infoday@Ioannina"
date:   2019-01-02 12:00:00 -0300
location: Ioannina, Greece
category: newsal
---


*FTESË*

Bashkia e Xumerkës së Veriut, ju fton të merrni pjesë në ditën e informacionit të projektit Phygital.

Eventi do të zhvillohet ditën e Premte, në datën 11 Janar, në orën 17:30, në rrugën K. Frontzos salla e kompleksit [“Frontzos Politeia”](group.frontzupolitia.gr/en/contact/) në Janinë.

*Para eventit, në orën 16:30 një konferencë për shtyp do të mbahet nga një prej promotorëve kryesorë të projektit. 

Projekti Phygital ka për qëllim zhvillimin dhe implementimin pilot të modeleve të prodhimit alternative me moton “dizenjo globalisht, prodho lokalisht”, të cilat bazohen në njohuritë e akumuluara nga histori globale suksesi, software dhe dizajn, me teknologji të prodhimit në rang lokal. Projekti implementohet në kuadrin e programit Ndërkombëtar të Bashkëpunimit "Interreg" “Ballkan – Mesdhe 2014-2020” me ente bashkëpunuese nga Greqia, Qiproja dhe Shqipëria.

*Infoday@Ioannina për axhendën*

[agenda - en]({{ "/assets/files/infoday-ioannina-Invitation_EN.pdf" | prepend: site.baseurl }})

[agenda - el]({{ "/assets/files/infoday-ioannina-Invitation_GR.pdf" | prepend: site.baseurl }})
