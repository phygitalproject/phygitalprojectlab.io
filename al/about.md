---
layout: info_page
langUrl: /al
title: Rreth Nesh
type: al-main
slug: aboutal
permalink: /al/about/
menus:
  al-main-nav:
    weight: 3
---

Fillesa e projektit ka qenë nevoja për gjetjen e një zgjidhjeje tekniko-ekonomike për një problem socio-institucional. Në kontekstin e zhvillimeve të fundit në sektorin socio-ekonomik, një numër gjithnjë dhe më i lartë njerëzish po marrin pjesë në projekte të inovacionit social, me qëllim krijimin e mundësive për tejkalimin e krizave të thella ekonomike, sociale dhe mjedisore në Europë. Megjithatë, këto komunitete përballen me probleme përsa i përket mundësisë për të fituar dhe për të përmbushur nevojat jetike duke u përfshirë në këto fusha inovative, si dhe sfida për të marrë mbështetjen e nevojshme organizative dhe qeveritare. Në të njëjtën kohë, përhapja e teknologjive krijuese inovatice dhe decentralizimi i burimeve të prodhimit të informacionit, ofrojnë një potencial ende të pashfrytëzuar për krijimin e mundësive të reja tekno-ekonomike.


Projekti PHYGITAL ka për qëllim të pilotojë, vlerësojë dhe promovojë linja prodhimi dhe modele biznesi inovative, duke u bazuar në eksperiencat më të suksesshme ndërkombëtare, si dhe nga projekte të tjera me burime të hapura dhe ku ka pasur përdorim të përbashkët teknologjie, si printerat 3D dhe makineritë CNC. Ky projekt i cili operon me moton “Dizenjo Globalisht, Prodho Lokalisht”, ka marrë shembullin e një numri të madh projektesh në rang global, ku pjesëmarrësit janë bashkuar për të ofruar një produkt të përbashkët dhe ku janë krijuar modele të reja të suksesshme biznesi. Projekti pritet që të rritë kapacitetet lokale për inovacione, duke shfrytëzuar eksperiencat globale për t’u përballur me sfidat lokale. Njëkohësisht, projekti do të nxitë bashkëpunimin ndërkombëtar, në mbështetje të sipërmarrjeve inovative dhe sipërmarrjeve sociale. 


PHYGITAL adapton një përqasje multidimensionale për të nxitur bashkëpunimin mes aktorëve dhe vendimmarrësve të ndryshëm si dhe të promovojë bashkëpunimin në nivel lokal, transnacional dhe global duke: (a) zhvilluar një platformë multilinguistike, ku do të vendosen eksperiencat e përbashkëta dhe dija e akumuluar nga puna e të gjithë pjesëmarrësve në projekt; (b) do të krijojë dhe do të mundësojë operimin e hapësirave të prodhimit të përbashkët (Makerspace), të cilat do të jenë të pajisura me makineritë e fjalës së fundit të teknologjisë për të mundësuar praktikat inovative në tre fushat tematike: agrikulturë (Greqi); praktikat e artit social (Qipro); & trashëgimnia kulturore (Shqipëri), si dhe (c) të mbështesë praktikat sipërmarrëse nëpërmjet rrjetit Phygital, një rrjet ky i decentralizuar për sipërmarrjet dhe profesionistët, të cilët i janë dedikuar inovacionit dhe sipërmarrjes sociale në nivel ndërkombëtar. 

 
Projekti PHYGITAL ka si target group sipërmarrësit inovativë dhe profesionistët e lirë; sipërmarrjet e vogla dhe të mesme që operojnë në rang lokal, dhe sipërmarrjet e vogla të sektoreve të ndryshëm, duke përfshirë dhe aktivistët, njerëzit që e kanë prodhimin një hobi të tyrin, por dhe anëtarët e tjerë të komunitetit që do të mund të shfrytëzojnë hapësirën dhe pajisjet e ofruara nga projekti. Vlera e shtuar e projektit qëndron në faktin se i jep mundësinë shumë njerëzve që të adaptojnë praktika profesionale, të cilat do të lejojnë krijimin e një impakti social pozitiv, duke hartuar dhe aplikuar zgjidhje të reja për sfida lokale dhe duke ndarë këto zgjidhje për benefit të publikut.   
