---
layout: post
title:  "Articles for the completion of Tzoumakers manufacturing workshops"
date:   2019-07-11 12:00:00 -0300
location: Ioannina, Greece
category: press
---

The ['typos-i'](https://typos-i.gr/article/oloklhrw8hkan-ta-ergasthria-kataskeyhs-twn-tzoumakers),  ['Ipirotikos Agon'](https://www.agon.gr/kathimerina/19333/oloklirothikan-ta-ergastiria-kataskeyis-ton-tzoumakers/),  ['ioannina24'](https://www.ioannina24.gr/%CE%B5%CE%B9%CE%B4%CE%B7%CF%83%CE%B5%CE%B9%CF%82/%CE%B7%CF%80%CE%B5%CE%B9%CF%81%CF%89%CF%84%CE%B9%CE%BA%CE%B7-%CE%B5%CF%80%CE%B9%CE%BA%CE%B1%CE%B9%CF%81%CE%BF%CF%84%CE%B7%CF%84%CE%B1/ipirotika_nea/488265-%CE%BF%CE%BB%CE%BF%CE%BA%CE%BB%CE%B7%CF%81%CF%8E%CE%B8%CE%B7%CE%BA%CE%B1%CE%BD-%CF%84%CE%B1-%CE%B5%CF%81%CE%B3%CE%B1%CF%83%CF%84%CE%AE%CF%81%CE%B9%CE%B1-%CE%BA%CE%B1%CF%84%CE%B1%CF%83%CE%BA%CE%B5%CF%85%CE%AE%CF%82-%CF%84%CF%89%CE%BD-tzoumakers),  ['epirusgate'](https://epirusgate.blogspot.com/2019/07/tzoumakers_12.html),  ['preveza24'](http://www.preveza24.gr/%CE%B5%CE%B9%CE%B4%CE%B7%CF%83%CE%B5%CE%B9%CF%82/%CE%B7%CF%80%CE%B5%CE%B9%CF%81%CF%89%CF%84%CE%B9%CE%BA%CE%B7-%CE%B5%CF%80%CE%B9%CE%BA%CE%B1%CE%B9%CF%81%CE%BF%CF%84%CE%B7%CF%84%CE%B1/ipirotika_nea/478697-%CE%B2-%CF%84%CE%B6%CE%BF%CF%85%CE%BC%CE%AD%CF%81%CE%BA%CE%B1-phygital-%CF%84%CE%B1-%CF%80%CF%81%CF%8E%CF%84%CE%B1-%CE%B5%CF%81%CE%B3%CE%B1%CF%83%CF%84%CE%AE%CF%81%CE%B9%CE%B1-%CE%BA%CE%B1%CF%84%CE%B1%CF%83%CE%BA%CE%B5%CF%85%CE%AE%CF%82-%CF%84%CF%89%CE%BD-tzoumakers),  ['proinos logos'](https://proinoslogos.gr/epikairotita/42-koinonia/54783-%CF%80%CF%81%CF%89%CF%84%CF%8C%CF%84%CF%85%CF%80%CE%BF-%CF%83%CE%BA%CE%B1%CF%80%CF%84%CE%B9%CE%BA%CF%8C-%CF%87%CE%B5%CE%B9%CF%81%CF%8C%CF%82-%CE%BA%CE%B1%CF%84%CE%B1%CF%83%CE%BA%CE%B5%CF%8D%CE%B1%CF%83%CE%B1%CE%BD-%CE%BF%CE%B9%E2%80%A6-tzoumakers) mentioned the successful completion of the third workshop of Phygital Project at open social lab of Tzoumerka.



