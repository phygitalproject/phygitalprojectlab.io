---
layout: post
title:  "Article on the 'Vimanews' online newspaper for the Information Workshop for the Phygital project"
date:   2019-01-14 12:00:00 -0300
location: Ioannina, Greece
category: press
---

The ['Vimanews'](http://www.vimanews.gr/) article devoted to the organization of the Workshop which was carried out by the Municipality of Northern Tzoumerka as part of the communication actions that constitute the Municipality's contribution to the implementation of the project. 

[Article link](http://www.vimanews.gr/index.php?option=com_content&view=article&id=32070:-phygital&catid=31:2014-05-27-08-58-16&Itemid=163)