---
layout: post
title:  "Article on 'pbnews' news portal for the Phygital project"
date:   2019-01-15 12:00:00 -0300
location: Ioannina, Greece
category: press
---

The ['pbnews'](http://www.pbnews.gr/) article mentioned the organization of the Workshop which was carried out by the Municipality of Northern Tzoumerka as part of the communication actions that constitute the Municipality's contribution to the implementation of the project and information about Phygital project.

[Article link](https://pbnews.gr/%CE%AD%CE%BD%CE%B1-%CE%BA%CE%B1%CE%B9%CE%BD%CE%BF%CF%84%CF%8C%CE%BC%CE%BF-%CE%B4%CE%B9%CE%B1%CE%BA%CF%81%CE%B1%CF%84%CE%B9%CE%BA%CF%8C-%CF%80%CF%81%CF%8C%CE%B3%CF%81%CE%B1%CE%BC%CE%BC%CE%B1/)