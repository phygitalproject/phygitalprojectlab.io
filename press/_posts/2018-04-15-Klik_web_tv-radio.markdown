---
layout: post
title:  "Klik tv article for the open technologies community in Tzoumerka"
date:   2018-04-15 12:00:00 -0300
location: Thessaloniki, Greece
category: press
---

The [Kliktv.gr](https://kliktv.gr) article devoted to the emergence and development of the open technologies community in Tzoumerka. 

[ Klik tv link (in greek)](https://kliktv.gr/%cf%84%ce%b6%ce%bf%cf%85%ce%bc%ce%ad%cf%81%ce%ba%ce%b1-%ce%b7-%ce%b3%ce%ad%ce%bd%ce%bd%ce%b7%cf%83%ce%b7-%ce%bc%ce%b9%ce%b1%cf%82-%ce%ba%ce%bf%ce%b9%ce%bd%cf%8c%cf%84%ce%b7%cf%84%ce%b1%cf%82-%ce%b1/)