---
layout: post
title:  "Article on voreiatzoumerka.gr for the Information Workshop for the Phygital project"
date:   2019-01-15 12:00:00 -0300
location: Ioannina, Greece
category: press
---

The ['voreiatzoumerka'](http://www.voreiatzoumerka.gr/) article devoted to the organization of the Workshop which was carried out by the Municipality of Northern Tzoumerka as part of the communication actions that constitute the Municipality's contribution to the implementation of the project. 

[Article link](http://www.voreiatzoumerka.gr/index.php/2015-09-08-12-10-48/2015-10-05-22-37-11/405-tzoumakers)