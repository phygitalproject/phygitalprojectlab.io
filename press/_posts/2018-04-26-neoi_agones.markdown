---
layout: post
title:  "Mention in the 'Neoi Agones' ('Young Strives') paper for the open technologies community in Tzoumerka"
date:   2018-04-26 12:00:00 -0300
location: Thessaloniki, Greece
category: press
---

The ['Neoi Agones' ('Young Strives')](http://neoiagones.gr/) article devoted to the emergence and development of the open technologies community in Tzoumerka. 

[![Newspaper excerpt]({{ "/assets/files/press_coverage/2018.04.26.NeoiAgones.png" | prepend: site.baseurl }})]({{ "/assets/files/press_coverage/2018.04.26.NeoiAgones.png" | prepend: site.baseurl }})