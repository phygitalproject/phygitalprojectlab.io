---
layout: post
title:  "Article on typos-i.gr for the Tzoumakers meeting in Kalentzi"
date:   2018-05-18 12:00:00 -0300
location: Tzoumerka, Greece
category: press
---

The ['typos-i.gr](https://typos-i.gr/) article relative to the first Tzoumakers meeting in Kalentzi. 

[Article link](https://typos-i.gr/article/kai-onoma-aytoy-tzoumakers)